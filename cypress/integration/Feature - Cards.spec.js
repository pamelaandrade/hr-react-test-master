/// <reference types="cypress" />
import cardsPage from '../support/pages/cardsPage'
const card = new cardsPage();
const testData = require("../fixtures/testData.json")

describe('Feature - Card', () => {

    before(function () {
        cy.visit('/')
    })   
    
    it('Add new card in each column', () => {
        card.addCard(1,testData.addCardToDo)
        card.addCard(2,testData.addCardprogress)
        card.addCard(3,testData.addCardDone)
    })

    it('Edit the last card in each column', () => {
        card.editCard(1,testData.editCardTex)
        card.editCard(2,testData.editCardTex)
        card.editCard(3,testData.editCardTex)
    })

    it('Delete the last card in each column', () => {
        card.deletecard(1)
        card.deletecard(2)
        card.deletecard(3)
    })

    it('Move card', () => {
        card.noveCard(1)

    })

})