/// <reference types="cypress" />
import columnPage from '../support/pages/columnPage'
const column = new columnPage();
const testData = require("../fixtures/testData.json")

describe('Feature - Column', () => {

    before(function () {
        cy.visit('/')
    })   
    
    it('Edit column title', () => {
        column.editTitleColumn(1,testData.titleColumn)
    })

})