/// <reference types="cypress" />
const element = require("../../fixtures/elements.json")
import '@4tw/cypress-drag-drop'


export default class cardsPage {

    addCard(value,text) {
        cy.xpath(`//*[@id="root"]/div/div[`+value+`]/button`).click()
        cy.get(`#root > div > div:nth-child(`+value+`) > div > div > div > div > textarea`).type(text)
        cy.get('button').contains('Add Card').click()
        cy.get('div').contains(text)
    }

    editCard(value,text){
        cy.get(`#root > div > div:nth-child(`+value+`) > div > div > div > div > button`).last().click({ force: true })
        cy.get(element.editCardText).clear().type(text)
        cy.get(element.editBoxButtonSave).click()
    }

    deletecard(value){
        cy.get(`#root > div > div:nth-child(`+value+`) > div > div > div > div > button`).last().click({ force: true })
        cy.get(element.editBoxButtonDelete).click()
    }

    noveCard(value) {        
        cy.get(`#root > div > div:nth-child(`+value+`) > div > div > div > div:nth-child(`+value+`)`)
        .drag(`#root > div > div:nth-child(`+(value+1)+`) > div > div > div> div:nth-child(`+value+`)`)
    }

}

 
 
