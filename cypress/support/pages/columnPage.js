const element = require("../../fixtures/elements.json")

export default class columnPage {
    editTitleColumn(value,text){
        cy.get(`#root > div > div:nth-child(`+value+`) > div > h4`).click({ force: true })
        cy.get(`#root > div > div:nth-child(`+value+`) > div > input`).clear().type(text).type('{enter}')
        cy.get(`#root > div > div:nth-child(`+value+`) > div > h4`).contains(text)
    }
}